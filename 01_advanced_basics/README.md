# log_analizer.py

Парсим лог вебсервера в файл html отчета.

## Алгоритм работы:

1. **read_conf_file()**
Читаем конфигурационный файл.

`[2021.09.12 13:42:35] I Config: {'REPORT_SIZE': '1001', 'REPORT_DIR': './reports', 'LOG_DIR': './log', 'REPORT_TMPL': 'report.html', 'REPORT_LOG': 'log_analyzer.log'}`

2. **select_last_log()**
Выбираем последний по дате в имени файл лога.

`[2021.09.12 13:42:35] I Last log file: ./log/nginx-access-ui.log-20170631.gz за 20170631`

Если такого нет, завершаем работу

3. Проверяем наличие файла отчета, если такой есть, завершаем работу.

`[2021.09.12 14:10:41] I Nothing to do: отчет ./reports/report-20170631.html already существует.`

4. **parse_log()**
Парсим лог: группируем по URL, подсчитываем строки, проверяем на ошибки.

`[2021.09.12 13:42:53] I Last log file: строк 2613774 за период 1927549 сек. - ошибок 88`

Если ошибок больше 10%, выходим.

5. **get_report()**
Генерируем отчет согласно заданию. Сортируем по максимальному времени, обрезаем согласно конфигу.

```
[2021.09.12 13:42:53] I Generating report...
[2021.09.12 13:42:54] I Cutting report to 1001 top lines sorting by max "time_sum".
```

6. **write_report()**
Записываем файл отчета в html файл.

`[2021.09.12 13:42:54] I Writing report to file ./reports/report-20170631.html with template report.html`

7. Заканчиваем работу.

`[2021.09.12 13:42:54] I Endinging report log.`

