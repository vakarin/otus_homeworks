#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


import argparse
import collections
import configparser
import gzip
import logging
import os
import re
import string
import sys
from datetime import datetime

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "REPORT_TMPL": "report.html"
}


def read_args():
    parser = argparse.ArgumentParser(
        description='This script parse log with config file.')
    parser.add_argument('-c', '--config', default='log_analizer.conf',
                        dest='conf_file', help="Configuration file", required=False)
    args = parser.parse_args()
    conf_file = args.conf_file

    return conf_file


def read_conf_file(conf_file):
    conf_dict = {}
    conf_ex = False

    if os.path.exists(conf_file):
        conf_parser = configparser.RawConfigParser()
        conf_parser.read(conf_file)
        try:
            conf_dict = dict(conf_parser.items('OPTIONS'))
        except (configparser.NoSectionError, KeyError) as expt:
            logging.exception(
                f'Conf: Нет секции "OPTIONS" - {expt} в {conf_file}', exc_info=True)
        conf_dict = {k.upper(): v for k, v in conf_dict.items()}

    return conf_dict


def update_config(conf_dict):

    config_loc = config.copy()
    config_loc.update(conf_dict)

    return config_loc


def select_last_log(log_dir):

    files = os.listdir(log_dir)
    regex_file_log = r'nginx-access-ui.log-(\d+)(.gz)?$'

    max_date = None
    res_log = False

    for i in files:
        if re.match(regex_file_log, i):
            log_date = re.search(regex_file_log, i).groups()[0]
            try:
                log_date_dt = datetime.strptime(log_date, '%Y%m%d')
            except (TypeError, ValueError) as expt:
                logging.exception(f'Last Log: {expt}', exc_info=True)
                continue
            max_date_dt = datetime.strptime(
                max_date, '%Y%m%d') if max_date else log_date_dt

            if log_date_dt >= max_date_dt:
                max_date = log_date
                res_log = i

    logs = collections.namedtuple('last_log', ['log_name', 'log_date'])
    last_log = logs(*(res_log, max_date) if res_log else ('', ''))

    return last_log


def parse_log(log_file_path):

    requests = {}
    line_all = 0
    line_err = 0
    time_all = 0
    regex_line_log = r'\S+ \S+  \S+ \[.+\] (".+?") \S+ \S+ ".+?" ".+?" ".+?" ".+?" ".+?" (\d+.\d+)'

    open_fun = gzip.open(log_file_path, 'rt') if log_file_path.endswith('.gz') \
        else open(log_file_path, 'r')

    with open_fun as file_log:
        for line_log in file_log:
            line_all += 1
            line_res = re.search(regex_line_log, line_log).groups()
            if line_res and len(line_res[0].split()) > 2:
                requests.setdefault(line_res[0].split()[1], list()).append(
                    float(line_res[1]))
                time_all += float(line_res[1])
            else:
                line_err += 1

    return requests, line_all, line_err, time_all


def get_report(requests, line_all, time_all, report_size):
    report = []
    logging.info(f'Generating report...')
    for key_req, val_req in requests.items():
        count_rec = len(val_req)
        count_perc = round(count_rec/line_all*100, 3)
        time_sum = round(sum(val_req), 3)
        time_avg = round(time_sum/count_rec, 3)
        time_max = max(val_req)
        time_med = val_req[count_rec//2]
        time_perc = round(time_sum/time_all, 3)

        report.append({"url": key_req, "count": count_rec, "count_perc": count_perc,
                       "time_avg": time_avg, "time_max": time_max, "time_med": time_med,
                       "time_perc": time_perc, "time_sum": time_sum})

    report = sorted(report, key=lambda item: item['time_sum'], reverse=True)
    report = report[:report_size]
    logging.info(
        f'Cutting report to {report_size} top lines sorting by max "time_sum".')

    return report


def write_report(file_template, path_rep_last, report):
    with open(file_template, 'r') as file_template:
        res_html = string.Template(
            file_template.read()).safe_substitute(table_json=report)

    with open(path_rep_last, 'w') as file_repo:
        file_repo.write(res_html)
    logging.info(
        f'Writing report to file {path_rep_last} with template {config.get("REPORT_TMPL")}')


def main():

    conf_file = read_args()

    conf_dict = read_conf_file(conf_file)

    config_loc = update_config(conf_dict)

    logging.basicConfig(filename=config_loc.get('REPORT_LOG'), filemode='w', level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

    logging.info('Starting report log.')
    logging.info(f'Config: {config_loc}')

    last_log = select_last_log(config_loc["LOG_DIR"])
    logging.info(
        f'Last log file: {config_loc["LOG_DIR"]}/{last_log.log_name} за {last_log.log_date}')

    path_log_last = f'{config_loc["LOG_DIR"]}/{last_log.log_name}'
    path_rep_last = f'{config_loc["REPORT_DIR"]}/report-{last_log.log_date}.html'

    if os.path.exists(path_rep_last):
        logging.info(
            f'Nothing to do: отчет {path_rep_last} already существует.')
        logging.info('Endinging report log.')
        sys.exit()

    if not last_log.log_name:
        logging.info(f'Nothing to do: No log files in {path_log_last}.')
        logging.info('Endinging report log.')
        sys.exit()

    requests, line_all, line_err, time_all = parse_log(path_log_last)
    logging.info(
        f'Last log file: строк {line_all} за период {round(time_all)} сек. - ошибок {line_err}')

    if line_err//line_all*100 > 10:
        logging.critical(
            f'Log file lines: ошибок парсинга {line_err} из {line_all} > 10%')
        logging.info('Endinging report log.')
        sys.exit()

    report = get_report(requests, line_all, time_all,
                        int(config_loc["REPORT_SIZE"]))

    write_report(config_loc.get('REPORT_TMPL'), path_rep_last, report)

    logging.info('Endinging report log.')


if __name__ == "__main__":
    main()
