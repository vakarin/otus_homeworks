#!/usr/bin/env python3

import log_analyzer
import os
import unittest

file_log_last, date_log_last = log_analyzer.select_last_log(
    log_analyzer.config["LOG_DIR"])
path_rep_last = f'{log_analyzer.config["REPORT_DIR"]}/report-{date_log_last}.html'

if os.path.exists(path_rep_last):
    os.remove(path_rep_last)


class TestStringMethods(unittest.TestCase):

    def test_select_last_log(self):
        self.assertEqual(log_analyzer.select_last_log(log_analyzer.config["LOG_DIR"]),
                         ('nginx-access-ui.log-20170630.gz', '20170630'))

    def test_main(self):
        self.assertEqual(log_analyzer.main(), None)

    def test_report_exist(self):
        self.assertTrue(os.path.exists(path_rep_last))


if __name__ == '__main__':
    unittest.main()
