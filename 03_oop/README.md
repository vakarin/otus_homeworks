# api.py

Реализован декларативный язык описания и система валидации запросов к HTTP API сервиса скоринга.

## Алгоритм работы:

Сервис по умолчанию работает на порту 8080.

1. **class CheckReqAndNull(object)**
Класс содержит проверку обязательности поля и может ли оно принимать пустые занчения.
Я вляется родительским для остальных классов валидации.

2. **class CharField(CheckReqAndNull)
**class ArgumentsField(CheckReqAndNull)**
**class EmailField(CharField)**
**class PhoneField(CheckReqAndNull)**
**class DateField(CheckReqAndNull)**
**class BirthDayField(DateField)**
**class GenderField(CheckReqAndNull)**
**class ClientIDsField(CheckReqAndNull)**
Классы валидации полей: аргументов, почтового адреса, телефонного номера, даты, дня рождения, пола, идентификатора поля клиента.

3. **class ClientsInterestsRequest(object)**
Класс валидации запроса интересов клиента.

4. **class OnlineScoreRequest(object)**
Класс валидации запроса скоринга.

5. **class MethodRequest(object)**
Класс валидации начального запроса.

6. **def check_auth(request)**
Функция аутентификации.

7. **def method_handler(request, ctx, store)**
функция определения метода запроса (запрос интересов или запрос скоринга).

8. **class MainHTTPHandler(BaseHTTPRequestHandler)**
Класс реализации HTTP сервера.

Более подробное описание полей и методов валидации вы можете найти в файле homework.pdf.

9. **Примеры запуска:**

**Запрос скоринга:**
`curl -X POST -H "Content-Type: application/json" -d '{"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "55cc9ce545bcd144300fe9efc28e65d415b923ebb6be1e19d2750a2c03e80dd209a27954dca045e5bb12418e7d89b6d718a9e35af34e14e1d5bcd5a08f21fc95", "arguments": {"phone": "79175002040", "email": "stupnikov@otus.ru", "first_name": "Стансилав", "last_name": "Ступников", "birthday": "01.01.1990", "gender": 1}}' http://127.0.0.1:8080/method/`

**Ответ:**
`[2021.11.29 09:48:26] I {'request_id': 'ce65f97825ab400da4a36be23deb3988', 'has': ['first_name', 'last_name', 'phone', 'email', 'birthday', 'gender'], 'response': {'score': 5.0}, 'code': 200}`

**Запрос на интересы пользователя:**
`curl -X POST -H "Content-Type: application/json" -d '{"account": "horns&hoofs", "login": "admin", "method": "clients_interests", "token": "45d18b8008df1201259faa7e92c83ed9cd7de0a4c0d1d52099b382c12ea06ddeed32c14009e3fdeeac36e2b4da370f94a4b558bd355b53ce708227267a430b1a", "arguments": {"client_ids": [1,2,3,4], "date": "20.07.2017"}}' http://127.0.0.1:8080/method/`

**Ответ:**
`[2021.11.29 10:39:24] I {'request_id': '31d4a07fc8fa4641b0805d7c1716036d', 'nclients': 4, 'response': {'1': ['cars', 'travel'], '2': ['geek', 'cars'], '3': ['otus', 'hi-tech'], '4': ['otus', 'travel']}, 'code': 200}`
