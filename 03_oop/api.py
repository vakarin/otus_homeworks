#!/usr/bin/env python3

import abc
import datetime
import hashlib
import json
import logging
import random
import re
import uuid
from http.server import BaseHTTPRequestHandler, HTTPServer
from optparse import OptionParser
from weakref import WeakKeyDictionary

from scoring import get_interests, get_score

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class CheckReqAndNull(object):

    def __init__(self, required, nullable):
        self._err = None
        self.data = WeakKeyDictionary()
        self.required = required
        self.nullable = nullable

    def __set_name__(self, owner, name):
        self._name = name

    def __get__(self, instance, owner):
        return self.data.get(instance, None), self._err

    def __set__(self, instance, value):
        self._err = None
        self.data[instance] = None

        if self.required and value == 'no_field':
            self.data[instance] = None
            self._err = f'"{self._name}" is required field'
            return

        if not self.nullable and value == '':
            self.data[instance] = None
            self._err = '"{self._name}" - fileld mast be not empty.'
            return


class CharField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if self._err:
            self.data[instance] = None
            return

        if isinstance(value, str):
            self.data[instance] = value
            self._err = None
        else:
            self.data[instance] = None
            self._err = f'"{self._name}": {value} is not a char string.'
            return

        if value == 'no_field':
            self.data[instance] = None


class ArgumentsField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if self._err:
            return

        if isinstance(value, dict):
            self.data[instance] = value
            self._err = None
        else:
            self.data[instance] = None
            self._err = f'"{self._name}": {value} is not a dict.'


class EmailField(CharField):

    def __set__(self, instance, value):
        super().__set__(instance, value)

        if value == 'no_field':
            self.data[instance] = None
            self._err = None
            return

        if not self._err:
            eml = re.compile(
                r"^[^.].+@([?)[a-zA-Z0-9-.])+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$")
            eml = eml.match(value)
            if eml:
                self.data[instance] = value
                self._err = None
            else:
                self.data[instance] = None
                self._err = f'"{self._name}": {value} is not email address.'


class PhoneField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)

        if value == 'no_field':
            self.data[instance] = None
            self._err = None
            return

        if isinstance(value, str) or isinstance(value, int):
            self.data[instance] = value
            self._err = None
        else:
            self.data[instance] = None
            self._err = f'"{self._name}": {value} is not a char or int string.'
            return

        if not self._err:
            phn = re.compile(r"^\+?[7]\d{10}$")
            phn = phn.match(str(value))
            if phn:
                self.data[instance] = value
                self._err = None
            else:
                self.data[instance] = None
                self._err = f'"{self._name}": {value} is not a phone number.'


class DateField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if self._err:
            return

        if value == 'no_field':
            self.data[instance] = None
            self._err = None
            return

        try:
            dt_obj = datetime.datetime.strptime(value, '%d.%m.%Y')
            self.data[instance] = value
            self._err = None
        except ValueError:
            self.data[instance] = None
            self._err = f'"{self._name}": {value} is not a date format DD.MM.YYYY.'


class BirthDayField(DateField):

    def __set__(self, instance, value):
        super().__set__(instance, value)

        if not self._err and self.data[instance]:
            self.brthd = datetime.datetime.strptime(value, '%d.%m.%Y')
            self.years = int(datetime.datetime.now().strftime("%Y")) \
                - int(self.brthd.strftime("%Y"))
            if self.years < 70:
                self.data[instance] = value
                self._err = None
            else:
                self.data[instance] = None
                self._err = f'{self.years} years - you too old...'


class GenderField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if self._err:
            return

        if value == 'no_field':
            self.data[instance] = None
            self._err = None
            return

        if value in [0, 1, 2]:
            self.data[instance] = value
            self._err = None
        else:
            self.data[instance] = None
            self._err = f'{value} - gender must be 0, 1 or 2.'


class ClientIDsField(CheckReqAndNull):

    def __set__(self, instance, value):
        super().__set__(instance, value)
        if self._err:
            return

        if isinstance(value, list):
            list_int = [isinstance(i, int) for i in value]
            if all(list_int):
                self.data[instance] = value
                self._err = None
                return

        self.data[instance] = None
        self._err = f'{value} is not a list of int.'


class ClientsInterestsRequest(object):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)

    def __init__(self, client_ids, date):
        self.client_ids = client_ids
        self.date = date

    def validate(self):
        inr_list = [self.client_ids, self.date]
        err_inr_list = [i[1] for i in inr_list]

        if not any(err_inr_list):
            inr_good = self.client_ids[0]
            inr_err = False
        else:
            inr_good = False
            inr_err = ','.join(filter(lambda i: i, err_inr_list))
        return inr_good, inr_err


class OnlineScoreRequest(object):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def __init__(self, first_name, last_name, email, phone, birthday, gender):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self.birthday = birthday
        self.gender = gender
        self.store = 0

    def validate(self):
        scr_list = [self.first_name, self.last_name,
                    self.phone, self.email, self.birthday, self.gender]

        val_scr_list = [i[0] for i in scr_list]
        err_scr_list = [i[1] for i in scr_list]

        if self.phone[0] and self.email[0]:
            scr_good = val_scr_list
            scr_err = err_scr_list
        elif self.first_name[0] and self.last_name[0]:
            scr_good = val_scr_list
            scr_err = err_scr_list
        elif self.birthday[0] and (self.gender[0] != None):
            scr_good = val_scr_list
            scr_err = err_scr_list
        else:
            scr_good = []
            scr_err = ','.join(filter(lambda i: i, err_scr_list))
        return scr_good, scr_err


class MethodRequest(object):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login[0] == ADMIN_LOGIN

    def __init__(self, account, login, token, arguments, method):

        self.account = account
        self.login = login
        self.token = token
        self.arguments = arguments
        self.method = method

    def validate(self):
        req_list = [self.account, self.login,
                    self.method, self.token, self.arguments]
        err_req_list = [i[1] for i in req_list]

        if not any(err_req_list):
            req_good = True
            req_err = False
        else:
            req_good = False
            req_err = ','.join(filter(lambda i: i, err_req_list))
        return req_good, req_err


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime(
            "%Y%m%d%H") + ADMIN_SALT).encode()).hexdigest()
    else:
        digest = hashlib.sha512((
            request.account[0] + request.login[0] + SALT).encode()).hexdigest()
    if str(digest) == str(request.token[0]):
        return True
    return False


def method_handler(request, ctx, store):
    response, code = None, None
    request = request['body']

    req_keys = ['account', 'login', 'method', 'token', 'arguments']
    req_dict = {i: request.get(i, 'no_field') for i in req_keys}
    req = MethodRequest(**req_dict)

    req_val = req.validate()
    req_good = req_val[0]
    req_err = req_val[1]

    if not req_good:
        return {req_err}, INVALID_REQUEST

    if not check_auth(req):
        return ERRORS[FORBIDDEN], FORBIDDEN

    method = req.method[0]
    arguments = req.arguments
    args = req.arguments[0]

    if method == 'online_score':
        if req.is_admin:
            response = {'score': int(ADMIN_SALT)}
            code = OK
            return response, code

        arg_keys = ['first_name', 'last_name',
                    'phone', 'email', 'birthday', 'gender']
        args = {i: args.get(i, 'no_field') for i in arg_keys}

        scr = OnlineScoreRequest(**args)

        scr_val = scr.validate()
        scr_good = scr_val[0]
        scr_err = scr_val[1]

        if scr_good and not any(scr_err):
            score = get_score(store, *scr_good)
            response = {'score': score}
            has = [arg_keys[i]
                   for i in range(len(arg_keys)) if scr_good[i] != None]
            ctx.update({'has': has})
            code = OK
        else:
            response = {'error': f'{scr_err}'}
            code = INVALID_REQUEST

    elif method == 'clients_interests':
        arg_keys = ['client_ids', 'date']
        args = {i: args.get(i, 'no_field') for i in arg_keys}

        inr = ClientsInterestsRequest(**args)

        inr_val = inr.validate()
        inr_good = inr_val[0]
        inr_err = inr_val[1]

        if inr_good:
            response = {str(i): get_interests(store, i) for i in inr_good}
            ctx.update({'nclients': len(inr_good)})
            code = OK
        else:
            response = {'error': f'{inr_err}'}
            code = INVALID_REQUEST

    else:
        code = BAD_REQUEST

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (
                self.path, data_string.decode(), context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(
                code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode())
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
