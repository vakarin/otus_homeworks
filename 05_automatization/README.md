# httpd.py

Разработан веб-сервер частично реализующий протокол HTTP.

## Алгоритм работы:

Сервер работает с сокетами и использует select для очередей событий.

```
Options:
  -s HOST, --host=HOST  # задаем ip (по умолчанию localhpst)
  -r ROOT, --root=ROOT  # Задаем ROOT_DOCUMENTS - корневую папку сервера (по умолчанию текущая)
  -p PORT, --port=PORT  # Задаем порт (по )
  -w WRKS, --wrks=WRKS  # Задаем количество воркеров (по умолчанию 4)
  -l LOG, --log=LOG     # Задаем файл лога (по умолчанию - выводим в консоль)
```

1. **def accept_wrapper(sock, root)**
Создает соединение из сокета.

2. **def service_connection(key, mask)**
Обслуживает очередь соединений.

3. **gef get_header(request_line, DOCUMENT_ROOT)**
Парсит заголовки HTTP.

4. **def get_response(request_method, path)**
Создает ответ в HTTP.

5. **def start(host, port, root, log)**
Предназначена для старта нескольких воркеров.

6. Пример запуска:

```
[2021.11.29 12:40:45.487] I 4 workers was started
[2021.11.29 12:40:45.527] I Starting server at 127.0.0.1:8080 at PID 2658303
[2021.11.29 12:40:45.527] I Starting server at 127.0.0.1:8080 at PID 2658302
[2021.11.29 12:40:45.533] I Starting server at 127.0.0.1:8080 at PID 2658301
[2021.11.29 12:40:45.533] I Starting server at 127.0.0.1:8080 at PID 2658304`
```

7. Результаты нагрузочного тестирования:

```
Server Software:        HTTP_Server
Server Hostname:        localhost
Server Port:            8080

Document Path:          /httptest/text..txt
Document Length:        5 bytes

Concurrency Level:      100
Time taken for tests:   10.453 seconds
Complete requests:      50000
Failed requests:        47
   (Connect: 0, Receive: 1, Length: 23, Exceptions: 23)
Total transferred:      5297562 bytes
HTML transferred:       249885 bytes
Requests per second:    4783.36 [#/sec] (mean)
Time per request:       20.906 [ms] (mean)
Time per request:       0.209 [ms] (mean, across all concurrent requests)
Transfer rate:          494.92 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       3
Processing:     1   21   1.0     21      27
Waiting:        0   21   1.0     21      27
Total:          4   21   1.0     21      27

```

8. Результты тестирования:
```
directory index file exists ... ok
document root escaping forbidden ... ok
Send bad http headers ... ok
file located in nested folders ... ok
absent file returns 404 ... ok
urlencoded filename ... ok
file with two dots in name ... ok
query string after filename ... ok
slash after filename ... ok
filename with spaces ... ok
Content-Type for .css ... ok
Content-Type for .gif ... ok
Content-Type for .html ... ok
Content-Type for .jpeg ... ok
Content-Type for .jpg ... ok
Content-Type for .js ... ok
Content-Type for .png ... ok
Content-Type for .swf ... ok
head method support ... ok
directory index file absent ... ok
large file downloaded correctly ... ok
post method forbidden ... ok
Server header exists ... ok

----------------------------------------------------------------------
Ran 23 tests in 0.018s

```