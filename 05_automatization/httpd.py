#!/usr/bin/env python3

import logging
import mimetypes
import multiprocessing
import os
import selectors
import socket
import sys
import types
from multiprocessing import Process
from optparse import OptionParser
from urllib.parse import unquote

OK = '200 OK'
BAD_REQUEST = '400 Bad Request'
FORBIDDEN = '403 Forbidden'
NOT_FOUND = '404 File Not Found'
methods = ['GET', 'HEAD']

sel = selectors.DefaultSelector()


def accept_wrapper(sock, root):
    conn, addr = sock.accept()
    logging.info(f"accepted connection from {addr} on PID {os.getpid()}")
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, root=root, inb=b"", outb=b"")
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)
    DOCUMENT_ROOT = data.root


def service_connection(key, mask):
    sock = key.fileobj
    data = key.data

    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)
        if recv_data:
            data.inb += recv_data
            if '\r\n\r\n' in recv_data.decode():
                request_line = data.inb.decode().splitlines()[0]
                request_method, path = get_header(request_line, data.root)
                data.outb = get_response(request_method, path)
                logging.info(f"data in connection to {data.addr}")
        else:
            sel.unregister(sock)
            sock.close()
            return

    if mask & selectors.EVENT_WRITE:
        if data.outb:
            sent = sock.send(data.outb)
            data.outb = data.outb[sent:]
            if not data.outb.decode():
                sel.unregister(sock)
                sock.close()
        else:
            logging.info(f"closing out connection to {data.addr}")
            sel.unregister(sock)
            sock.close()


def get_header(request_line, DOCUMENT_ROOT):
    request_line = request_line.rstrip('\r\n')
    try:
        (request_method, path, request_version) = request_line.split()
    except ValueError:
        return None, BAD_REQUEST

    if request_method not in methods:
        return None, BAD_REQUEST

    path = unquote(path)
    path = path.split('?')[0]

    path_real = os.path.join(DOCUMENT_ROOT, path.lstrip('/'))
    if os.path.isdir(path_real):
        path = os.path.join(path_real, 'index.html')
    else:
        path = path_real

    if not os.path.isfile(path):
        return request_method, NOT_FOUND

    path = os.path.abspath(path)
    if not path.startswith(DOCUMENT_ROOT):
        return request_method, FORBIDDEN

    return request_method, path


def get_response(request_method, path):

    if path == BAD_REQUEST:
        status = BAD_REQUEST
        page = 'Error 400: Bad request'
    elif path == NOT_FOUND:
        status = NOT_FOUND
        page = 'Error 404: File not found'
    elif path == FORBIDDEN:
        status = FORBIDDEN
        page = 'Error 403: Forbidden'

    mimetype = mimetypes.guess_type(path)[0]

    if path in [BAD_REQUEST, NOT_FOUND, FORBIDDEN]:
        page = f'<html><body><center><h3>{page}</h3></center></body></html>'.encode()
        page_length = len(page)
    elif request_method == 'HEAD':
        status = OK
        page = b'\r\n\r\n'
        page_length = os.path.getsize(path)
    else:
        with open(path, 'rb') as f:
            status = OK
            page = f.read()
            page_length = len(page)

    response = f'HTTP/1.1 {status}\r\n'
    response += 'Date: 23.10.2021\r\n'
    response += 'Server: HTTP_Server\r\n'
    response += f'Content-Length: {page_length}\r\n'
    response += f'Content-Type: {mimetype}\n\n'
    response = response.encode() + page

    return response


def start(host, port, root, log):

    logging.basicConfig(filename=log, level=logging.INFO,
                        format='[%(asctime)s.%(msecs)03d] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    lsock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
    lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
    lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    lsock.bind((host, port))
    lsock.listen()
    logging.info(f"Starting server at {host}:{port} at PID {os.getpid()}")
    lsock.setblocking(False)
    sel.register(lsock, selectors.EVENT_READ, data=None)

    DOCUMENT_ROOT = root

    try:
        while True:
            events = sel.select(timeout=0)
            for key, mask in events:
                if key.data is None:
                    accept_wrapper(key.fileobj, DOCUMENT_ROOT)
                else:
                    service_connection(key, mask)
    except KeyboardInterrupt:
        logging.info("caught keyboard interrupt, exiting")
    finally:
        sel.close()
        lsock.close()


if __name__ == "__main__":

    op = OptionParser()
    op.add_option("-s", "--host", action="store",
                  type=str, default='127.0.0.1')
    op.add_option("-r", "--root", action="store",
                  type=str, default=os.getcwd())
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-w", "--wrks", action="store", type=int, default=2)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()

    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s.%(msecs)03d] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

    DOCUMENT_ROOT = opts.root
    host = opts.host
    port = opts.port
    log = opts.log

    multiprocessing.set_start_method('spawn')

    workers = []
    for index in range(opts.wrks):
        worker = Process(target=start, args=(host, port, DOCUMENT_ROOT, log),)
        worker.daemon = True
        worker.start()
        workers.append(worker)
    logging.info(f"{len(workers)} workers was started")

    try:
        while True:
            pass
            for worker in workers:
                if not worker.is_alive():
                    logging.info(f"{worker.name} died")

    except KeyboardInterrupt:
        logging.info("caught keyboard interrupt, exiting")
